import helpers.CrossoverHelper
import helpers.MutateHelper
import helpers.sumUpTo
import models.Chromosome
import java.util.*

const val populationSize = 50
const val maxIterationsCount = 1000
const val chromosomeSize = 12
const val crossoverThreshold = 0.6
const val mutationProbabilityThreshold = 0.01
const val debug = false

val maxClashes = (0 until chromosomeSize).sum()

val random = Random()

fun main(args: Array<String>) {
    var population: MutableList<Chromosome> = generateRandomPopulation()
    var best = maxClashes
    var bestIteration = 0
    var i = 0
    while (best != 0) {
        if (i > maxIterationsCount) {
            break
        }
        printStartOfIteration(i)
        val parents = selectParents(population)
        val children = crossover(parents)
        population = mutate(children)
        val fitnesses = population.map { it to fitness(it) }.sortedBy { it.second }.reversed()
        val bestChromosome = fitnesses.first().first
        val bestChromosomeValue = bestChromosome.value()
        if (best > bestChromosomeValue) {
            println("[Main] New best $bestChromosome at iteration no. ${i + 1}")
            best = bestChromosomeValue
            bestIteration = i
        }
        println("[Main] Best chromosome is $bestChromosome (f(x) = ${fitnesses.first().second})")
        println("[Main] Distance to all time best: ${bestChromosomeValue - best}")
        i += 1
    }
    println("[Main] All time best value is $best in iteration no. ${bestIteration + 1}")
    printEnd()
}

fun mutate(children: List<Chromosome>): MutableList<Chromosome> {
    val mutable = children.toMutableList()
    for (i in 0 until children.size) {
        mutable[i] = MutateHelper.mutate(children[i])
    }
    return mutable
}

fun crossover(parents: List<Chromosome>): List<Chromosome> {
    val children = mutableListOf<Chromosome>()
    val shuffled = parents.shuffled()
    val firstHalf = shuffled.take(populationSize / 2)
    val secondHalf = shuffled.takeLast(populationSize / 2)
    val pairs = firstHalf.mapIndexed { index, chromosome -> chromosome to secondHalf[index] }
    pairs.forEach {
        if (random.nextDouble() < crossoverThreshold) {
            val results = CrossoverHelper.crossover(it)
            children.add(results.first)
            children.add(results.second)
        } else {
            children.add(it.first)
            children.add(it.second)
        }
    }
    while (children.size < populationSize) {
        children.add(parents[random.nextInt(parents.size)])
    }
    return children
}

private fun printEnd() {
    println("---------------------------")
    println("---------- END ------------")
    println("---------------------------")
}

private fun printStartOfIteration(i: Int) {
    println("---------------------------")
    println("---- Iteration no. ${String.format("%03d", i + 1)} ----")
    println("---------------------------")
}

fun generateRandomPopulation(): MutableList<Chromosome> {
    val population = mutableListOf<Chromosome>()
    val possibleValues = 0 until chromosomeSize
    for (i in 0 until populationSize) {
        val chromosome = Chromosome(possibleValues.shuffled())
        debugPrint("[Generate] Generated $chromosome")
        population.add(chromosome)
    }
    return population
}

fun selectParents(population: List<Chromosome>): List<Chromosome> {
    val parents = mutableListOf<Chromosome>()
    val sumOfValues = population.map { fitness(it) }.reduce { acc, i -> acc + i }
    val probabilities = population.map { probability(it, sumOfValues) }
    for (i in 0 until population.size) {
        val randomValue = random.nextDouble()
        val value = Math.min(randomValue, probabilities.sumUpTo(probabilities.size))

        probabilities.forEachIndexed { index, _ ->
            if (value > probabilities.sumUpTo(index - 1) && value <= probabilities.sumUpTo(index)) {
                debugPrint("[Parents] Chosen parent at index $index with probability ${probabilities[index]}: ${population[index]}")
                parents.add(population[index])
            }
        }

    }
    return parents
}

fun debugPrint(message: String) {
    if (debug) {
        println(message)
    }
}

fun probability(chromosome: Chromosome, sumOfValues: Int): Double {
    return fitness(chromosome) / sumOfValues.toDouble()
}

fun fitness(chromosome: Chromosome): Int {
    return maxClashes - chromosome.value()
}