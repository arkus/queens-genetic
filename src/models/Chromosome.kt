package models

import kotlin.math.abs

data class Chromosome(val data: List<Int>) {
    fun value(): Int {
        var clashes = 0

        val rowColumnClashes = abs(data.size - data.distinct().size)
        clashes += rowColumnClashes

        for (i in 0 until data.size) {
            for (j in 0 until data.size) {
                if (i != j) {
                    val dx = abs(i - j)
                    val dy = abs(data[i] - data[j])
                    if (dx == dy) {
                        clashes += 1
                    }
                }
            }
        }

        return clashes
    }

    override fun toString(): String {
        return "${this.data} (${this.value()})"
    }
}