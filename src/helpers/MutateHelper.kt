package helpers

import debugPrint
import models.Chromosome
import mutationProbabilityThreshold
import java.util.Random

object MutateHelper {
    private val random = Random()

    fun mutate(chromosome: Chromosome): Chromosome {
        val values = chromosome.data.toMutableList()
        for (i in 0 until values.size) {
            if (random.nextDouble() < mutationProbabilityThreshold) {
                val index1 = random.nextInt(chromosome.data.size)
                val index2 = random.nextInt(chromosome.data.size)
                val tmp = values[index1]

                values[index1] = values[index2]
                values[index2] = tmp
            }
        }

        val mutated = Chromosome(values)

        if (chromosome.data != values) {
            debugPrint("[Mutate] Result: $chromosome -> $mutated")
        }

        return mutated
    }

}