package helpers

import debugPrint
import models.Chromosome
import java.util.Random
import kotlin.math.max
import kotlin.math.min

object CrossoverHelper {
    private val random = Random()

    fun crossover(pair: Pair<Chromosome, Chromosome>): Pair<Chromosome, Chromosome> {
        val child1 = generateChild(pair.first, pair.second)
        val child2 = generateChild(pair.second, pair.first)

        val result = child1 to child2

        debugPrint("[Crossover] Crossover with parents $pair -> $result")
        return result
    }

    private fun generateChild(parent1: Chromosome, parent2: Chromosome): Chromosome {
        val point = random.nextInt(parent1.data.size)
        val point2 = random.nextInt(parent1.data.size)
        val values1 = parent1.data.subList(min(point, point2), max(point, point2))
        val values2 = parent2.data.toMutableList()
        values2.removeIf { values1.contains(it) }
        values1.forEachIndexed { index, i ->
            values2.add(index + min(point, point2), i)
        }
        return Chromosome(values2)
    }
}